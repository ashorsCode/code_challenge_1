﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CodeChallenge.Models;
using System.Net.Http;
using System.Text;


/// <summary>
/// Project uses ASP.NET Core 2.1 MVC // Takes around 380 seconds and does not pass the test.
/// After matrices are processed, there will be a screen showing the time taken and final message.
/// </summary>
namespace CodeChallenge.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// Action below performs the whole test
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            string result = "";

            HttpClient client = new HttpClient();

            //Used for data set init api call
            DataSetInit dataSetInit = new DataSetInit();

            Stopwatch stopwatch = Stopwatch.StartNew();

            HttpResponseMessage response = await client.GetAsync("https://recruitment-test.investcloud.com/api/numbers/init/1000");

            if (response.IsSuccessStatusCode)
            {
                dataSetInit = await response.Content.ReadAsAsync<DataSetInit>();

                //Code below performs actions to save dataset content for A and B containers
                var dataSetATask = SetDataSetValues(client, response, "https://recruitment-test.investcloud.com/api/numbers/A/row/", dataSetInit.Value);
                var dataSetBTask = SetDataSetValues(client, response, "https://recruitment-test.investcloud.com/api/numbers/B/row/", dataSetInit.Value);

                int[][] dataSetA = (await Task.WhenAll(dataSetATask)).SelectMany(x => x).ToArray();
                int[][] dataSetB = (await Task.WhenAll(dataSetBTask)).SelectMany(x => x).ToArray();

                //Multiplying of matrices is done and hashed
                string matrixStringResult = MultiplyMatrices(dataSetA, dataSetB, dataSetInit.Value);

                //Testing to see if the hash is correct and returns a message based on the validity message from the api
                result = await CheckResultOfMatrixHash(matrixStringResult, client, response);
            }

            //Stopwatch is stopped and time taken to compute is calculated
            stopwatch.Stop();
            long timeTaken = stopwatch.ElapsedMilliseconds / 1000;

            //Model holds data to be shown on index/ home page after processing
            HomePage homePage = new HomePage
            {
                Message = result,
                TimeTaken = timeTaken
            };

            return View(homePage);
        }

        /// <summary>
        /// Sets the data for A and B matrices
        /// </summary>
        /// <param name="client">Performs api call</param>
        /// <param name="response">Response from api call</param>
        /// <param name="urlPath">Url path for api call</param>
        /// <param name="rowColSize">Size of the row/col used for matrices</param>
        /// <returns>returns matrix</returns>
        private async Task<int[][]> SetDataSetValues(HttpClient client, HttpResponseMessage response, string urlPath, int rowColSize)
        {
            DataSet dataSet = new DataSet();
            int[][] newData = new int[rowColSize][];

            for (int i = 0; i < rowColSize; i++)
            {
                response = await client.GetAsync(urlPath + i);

                if (response.IsSuccessStatusCode)
                {
                    //Holds response from api call
                    dataSet = await response.Content.ReadAsAsync<DataSet>();
                }

                //Adds array of 1000 items to row of matrix from api call
                newData[i] = dataSet.Value;
            }

            return newData;
        }

        /// <summary>
        /// Multiplies both matrices, creates result string, then hashes results string
        /// </summary>
        /// <param name="matrixA">Matrix A</param>
        /// <param name="matrixB">Matrix B</param>
        /// <param name="rowColSize">Size of the row/col used for matrices</param>
        /// <returns>returns a hashed matrix string</returns>
        private string MultiplyMatrices(int[][] matrixA, int[][] matrixB, int rowColSize)
        {
            string joinedMatrixResult = "";
            string hashedMatrix = "";
            int temp = 0; //holds temporary value for current matrix position being computed

            //Computes multiplication of matrices by splitting it into different threads, making the process faster
            Parallel.For(0, rowColSize, i =>
            {
                for (int j = 0; j < rowColSize; j++)
                {
                    temp = 0;

                    //Calculates the final value for the current position of the matrix
                    for (int k = 0; k < rowColSize; k++)
                    {
                        temp += matrixA[i][k] * matrixB[k][j];
                    }

                    //Adds final computed value to matrix result string
                    joinedMatrixResult += temp.ToString();
                }
                       
            });

            //Hashes final computed result string
            hashedMatrix = HashResultMatrix(joinedMatrixResult);

            return hashedMatrix;
        }

        /// <summary>
        /// Hashes final result string
        /// </summary>
        /// <param name="joinedMatrixResult">Final hashed matrix result string</param>
        /// <returns>returns hashed result string</returns>
        private string HashResultMatrix(string joinedMatrixResult)
        {
            using(System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(joinedMatrixResult);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();

                for(int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }

                return sb.ToString();
            }
        }

        /// <summary>
        /// Checks to see if hash is correct
        /// </summary>
        /// <param name="hashedMatrix">Hashed result string</param>
        /// <param name="client">Client to perform api call</param>
        /// <param name="response">Variable to hold reponse of api call</param>
        /// <returns>returns message saying if hash is correct or not</returns>
        private async Task<string> CheckResultOfMatrixHash(string hashedMatrix, HttpClient client, HttpResponseMessage response)
        {
            Results results = new Results();

            //model to send hash string
            SendHash sendHash = new SendHash
            {
                String = hashedMatrix
            };

            response = await client.PostAsJsonAsync("https://recruitment-test.investcloud.com/api/numbers/validate", sendHash);

            if (response.IsSuccessStatusCode)
            {
                results = await response.Content.ReadAsAsync<Results>();
            }

            return results.Value;
        }

    }
}
