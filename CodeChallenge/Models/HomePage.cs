﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeChallenge.Models
{
    /// <summary>
    /// Holds values for home page after computing
    /// </summary>
    public class HomePage
    {
        public long TimeTaken { get; set; }

        public string Message { get; set; }
    }
}
