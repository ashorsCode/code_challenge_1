﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeChallenge.Models
{
    /// <summary>
    /// Holds result content when testing hash with api call
    /// </summary>
    public class Results
    {
        public string Value { get; set; }

        public string Cause { get; set; }

        public bool Success { get; set; }
    }
}
