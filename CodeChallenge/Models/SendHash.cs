﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeChallenge.Models
{
    /// <summary>
    /// Used to send hashed string to api call
    /// </summary>
    public class SendHash
    {
        public string String { get; set; }
    }
}
