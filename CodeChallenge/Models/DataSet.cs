﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeChallenge.Models
{
    /// <summary>
    /// Holds data set content from api call
    /// </summary>
    public class DataSet
    {
        public int[] Value { get; set; }

        public string Cause { get; set; }

        public bool Success { get; set; }
    }
}
